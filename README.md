# Vocab

 - SCS: Solid Community Server
 - CSS: Cascading Style Sheets

# Main override components

This components override SCS's `main.ejs` html and css.

Currently, its used as a ducktap solution to match the style of SCS webpage ( such as /idp/register/ )
to Penny integrated UI's CSS.

Later the goal will be to abstract this to any UI and CSS.

# Install

`cd` to the root of your SCS instance, where the `config.json` file is located

```
git clone https://gitlab.com/liquid-solid/tailwind-main
cd tailwind-main
npm i
npm run build
```

add to your componentsjs `config.json`, at the end of the `import` array:

```
    "./tailwind-main/default.json"
```

# Notes

 - Doesn't override default SCS default index.html ( assume the UI override it )
 - Only tested on /idp/register/ page
 - DI: better to override? should the original import be removed?
